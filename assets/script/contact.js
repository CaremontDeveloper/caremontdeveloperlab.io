const common = require('./_common');

const emailRegex = new RegExp('.+@.+');
const phoneNumberRegex = new RegExp('[\d]*');

const showUserMessage = function (message, isShown) {
    const userMessage = document.querySelector('.submit-order-user-message');

    userMessage.innerHTML = message;
    userMessage.classList.toggle('is-shown', isShown);
};

const onSubmitOrder = function () {
    const name = document.querySelector('.submit-order-name').value;
    if (!name || name.length < 3)
        return showUserMessage('You must enter a valid name, please enter your name and try again.', true);

    const email = document.querySelector('.submit-order-email').value;
    if (!email || email.length < 3 || !emailRegex.exec(email))
        return showUserMessage('You must enter a valid email, please enter your email and try again.', true);

    const phoneNumber = document.querySelector('.submit-order-phone-number').value;
    if (!phoneNumber || phoneNumber.length < 11 || !phoneNumberRegex.exec(phoneNumber))
        return showUserMessage('You must enter a valid phone number, please enter your phone number and try again.', true);

    const message = document.querySelector('.submit-order-message').value;
    if (!message || message.length < 3)
        return showUserMessage('You must enter a valid order, please enter your order and try again.', true);

    showUserMessage('', false);
};

const onLoad = function () {
    const submitOrderButton = document.querySelector('.submit-order-button');

    submitOrderButton.addEventListener('click', onSubmitOrder);
};

window.addEventListener('load', onLoad);
