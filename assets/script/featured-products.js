const onExpand = function (element) {
    element.classList.toggle('is-expanded');
};

const onLoad = function () {
    const moreTextExpanders = document.querySelectorAll('.more-text-expand');

    moreTextExpanders.forEach(element => {
        element.addEventListener('click', () => onExpand(element));
    });
};

window.addEventListener('load', onLoad);