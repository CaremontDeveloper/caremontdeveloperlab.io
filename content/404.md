{
    "url": "404.html",
    "title": "Caremont | 404 - Not Found",
    "description": "We create a fun and easy learning environment that utilizes progressive teaching techniques proven to help students learn better.",
    "layout": "404",
    "styles": [],
    "scripts": []
}
