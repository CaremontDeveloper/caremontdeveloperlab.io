{
    "title": "Caremont | What We Do",
    "description": "We create a fun and easy learning environment that utilizes progressive teaching techniques proven to help students learn better.",
    "layout": "what-we-do",
    "styles": [],
    "scripts": []
}
