{
    "title": "Caremont | About",
    "description": "We create a fun and easy learning environment that utilizes progressive teaching techniques proven to help students learn better.",
    "layout": "_index",
    "styles": [
        "_index"
    ],
    "scripts": []
}
