const load = function () {
    window.addEventListener('scroll', (event) => {
        const scrollThreshold = 10;
        const isScrolling =
            document.body.scrollTop > scrollThreshold
            || document.documentElement.scrollTop > scrollThreshold;

        document.body.classList.toggle('is-scrolling', isScrolling);
    });
};

window.addEventListener('load', load);
